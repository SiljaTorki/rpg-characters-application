﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class Ranger : Character
    {
        /// <summary>
        /// 
        /// Constructor for character subclass - Ranger
        /// Sets the base attributes, weapon, armor for Ranger
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="level"></param>
        public Ranger(string name = "undefined", int level = 1) : base(name, level, new PrimaryAttributes(1, 7, 1))
        {
            baseAttributes = new PrimaryAttributes(1, 7, 1);
            //Set base weapons
            Weapon bow = new Weapon(WeaponsENUM.BOW, 6, 3, 1);

            weapons = new List<Weapon> { bow };

            //Set base Armor
            Armor mail = new Armor(ArmorENUM.MAIL, new PrimaryAttributes(1, 3, 2), 1);
            Armor leather = new Armor(ArmorENUM.LEATHER, new PrimaryAttributes(4, 2, 1), 1);
            armorTypes = new List<Armor> { mail, leather };

            //Set Equipment
            equipmentItems = new Dictionary<SlotENUM, Item> { { SlotENUM.HEAD, null }, { SlotENUM.BODY, null }, { SlotENUM.LEGS, null }, { SlotENUM.WEAPON, null } };

        }

        /// <summary>
        /// 
        /// Inherit abstract method from Character.
        /// Update primary attributes to character and goes up a level.
        /// Level only increases by 1
        /// 
        /// </summary>
        /// <returns></returns>
        public override PrimaryAttributes getCharacterLevelup()
        {
            PrimaryAttributes levelUp = new PrimaryAttributes(1, 5, 1);
            baseAttributes += levelUp;
            return baseAttributes;
        }
    }
}
