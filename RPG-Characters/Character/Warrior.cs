﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class Warrior : Character
    {
        /// <summary>
        /// 
        /// Constructor for character subclass - Warrior
        /// Sets the base attributes, weapon, armor for Warrior
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="level"></param>
        public Warrior(string name = "undefined", int level = 1) : base(name, level, new PrimaryAttributes(5, 2, 1))
        {
            baseAttributes = new PrimaryAttributes(5, 2, 1);
            //Set base weapons
            Weapon axe = new Weapon(WeaponsENUM.AXE, 0, 0, 1);
            Weapon hammer = new Weapon(WeaponsENUM.HAMMER, 0, 0, 1);
            Weapon sword = new Weapon(WeaponsENUM.SWORD, 0, 0, 1);

            weapons = new List<Weapon> { axe, hammer, sword };

            //Set base Armor
            Armor mail = new Armor(ArmorENUM.MAIL, new PrimaryAttributes(1, 3, 2), 1);
            Armor plate = new Armor(ArmorENUM.PLATE, new PrimaryAttributes(1, 3, 2), 1);
            armorTypes = new List<Armor> { mail, plate };

            //Set Equipment
            equipmentItems = new Dictionary<SlotENUM, Item> { { SlotENUM.HEAD, null }, { SlotENUM.BODY, null }, { SlotENUM.LEGS, null }, { SlotENUM.WEAPON, null } };

        }

        /// <summary>
        /// 
        /// Inherit abstract method from Character.
        /// Update primary attributes to character and goes up a level.
        /// Level only increases by 1
        /// 
        /// </summary>
        /// <returns></returns>
        public override PrimaryAttributes getCharacterLevelup()
        {
            PrimaryAttributes levelUp = new PrimaryAttributes(3, 2, 1);
            baseAttributes += levelUp;
            return baseAttributes;
        }
    }
}
