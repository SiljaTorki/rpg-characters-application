﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class Mage : Character
    {
        /// <summary>
        /// 
        /// Constructor for character subclass - Mage
        /// Sets the base attributes, weapon, armor for Mage
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="level"></param>
        public Mage(string name = "undefined", int level = 1) : base(name, level, new PrimaryAttributes(1, 1, 8))
        {
            baseAttributes = new PrimaryAttributes(1, 1, 8);
            //Set base weapons
            Weapon staff = new Weapon(WeaponsENUM.STAFF, 3, 2, 1);
            Weapon wand = new Weapon(WeaponsENUM.WAND, 2, 1, 1);

            weapons = new List<Weapon> { staff, wand };

            //Set base Armor
            Armor cloth = new Armor(ArmorENUM.CLOTH, new PrimaryAttributes(1, 1, 1), 1);
            armorTypes = new List<Armor> { cloth };

            //Set Equipment
            equipmentItems = new Dictionary<SlotENUM, Item> { { SlotENUM.HEAD, null }, { SlotENUM.BODY, null }, { SlotENUM.LEGS, null }, { SlotENUM.WEAPON, null } };

        }

        /// <summary>
        /// 
        /// Inherit abstract method from Character.
        /// Update primary attributes to character and goes up a level.
        /// Level only increases by 1
        /// 
        /// </summary>
        /// <returns></returns>
        public override PrimaryAttributes getCharacterLevelup()
        {
            PrimaryAttributes levelUp = new PrimaryAttributes(1, 1, 5);
            baseAttributes += levelUp;
            return baseAttributes;
        }
    }
}
