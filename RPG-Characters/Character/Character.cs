﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public abstract class Character
    {
        private string Name { get; set; }
        public int Level { get; set; }
        private PrimaryAttributes PrimaryAttributes;
        public List<Weapon> weapons { get; set; }
        public List<Armor> armorTypes { get; set; }

        //Store equitment as a Dictionary<Slot, Item>
        public Dictionary<SlotENUM, Item> equipmentItems { get; set; }
        public abstract PrimaryAttributes getCharacterLevelup();
        public PrimaryAttributes baseAttributes { get; set; }
        public PrimaryAttributes totalAttributes { get; set; }
        private double totalAttribute;


        /// <summary>
        /// 
        /// Constructor for Character abstract class
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="level"></param>
        /// <param name="primaryAttributes"></param>
        public Character(string name, int level, PrimaryAttributes primaryAttributes)
        {
            this.Name = name;
            this.Level = level;
            this.PrimaryAttributes = primaryAttributes;
        }

        /// <summary>
        /// 
        /// Method to equip any item.
        /// If a character tries to equip a weapon or a armor they should not be able to, 
        /// either by it being the wrong type or being too high of a level requirement, then a custom InvallidLevel, InvalidWeaponException or InvalidArmorException is thrown.
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns> string </returns>
        public string EquipItem(Item item)
        {
            //return string
            string writeItem = "";

            //Checks for exeptions
            if(this.Level < item.RequierdLevel)
            {
                throw new InvallidLevel("Level exeption");
            } else if (item.Slot != SlotENUM.WEAPON && item.Name.Contains("Weapon"))
            {
                throw new InvalidWeaponException("Weapon exeption");
            } else if (item.Slot == SlotENUM.WEAPON && item.Name.Contains("Armor"))
            {
                throw new InvalidArmorException("Armor exeption");
            } else
            {
                //Add Item to Character either as Weapon or Armor
                if (item.Slot == SlotENUM.WEAPON)
                {
                    Weapon equipWeapon = (Weapon)item;
                    weapons.Add(equipWeapon);
                    writeItem = "New weapon equipped!";
                }
                else
                {
                    Armor equipArmor = (Armor)item;
                    armorTypes.Add(equipArmor);
                    writeItem = "New armour equipped!";
                }
            }

            return writeItem;
        }

        /// <summary>
        /// 
        /// Method to calculate Total attribute.
        /// 
        /// Total attribute = attributes from level + attributes from all equipped armor
        /// 
        /// </summary>
        /// <returns> double </returns>
        public double TotalAttribute()
        {
            //Iterates through Dictionary<SlotENUM, Item> equipmentItems
            foreach (KeyValuePair<SlotENUM, Item> equipment in equipmentItems)
            {
                //For every Armor item sum primary attributes and add ti total attributes
                if (equipment.Key != SlotENUM.WEAPON && equipment.Value != null)
                {
                    //Get the valut from the equipted armor
                    Armor totArmor = (Armor)equipment.Value;
                    this.totalAttributes = new PrimaryAttributes(totArmor.Attributes.strength, totArmor.Attributes.dexterinty, totArmor.Attributes.intelligense);
                }

            }
            //the total is what armor is equipped 
            //and all the primary attributes present in those items to the base
            double sumBase = this.baseAttributes.strength + this.baseAttributes.dexterinty + this.baseAttributes.intelligense;
            double tot = this.totalAttributes.strength + this.totalAttributes.dexterinty + this.totalAttributes.intelligense;
            totalAttribute = sumBase + tot;
            return totalAttribute;
        }

        /// <summary>
        /// 
        /// Method to calculate Character damage
        /// 
        /// Character damage = Weapon DPS * (1 + TotalPrimaryAttribute/100)
        /// </summary>
        /// <returns> double </returns>
        public double characterDemage()
        {
            //Determine the character’s DPS
            double DPS = 1; // default for the character witch haven't collected any items
            double sum = 0;
            
            // Sum all DPS for evry weapon on Character
            this.weapons.ForEach(w => sum += w.DPS());
            if(sum > DPS)
            {
                DPS = sum;
            }

            //return total demage formula
            return DPS * (1 + this.totalAttribute / 100);
        }

        /// <summary>
        /// 
        /// Method to display their statistics to a character sheet.
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <returns> string </returns>
        public string showCharacter(Character c)
        {
            //Display character statistics to a character sheet -> generated by StringBuider
            StringBuilder s = new StringBuilder("Character stats display");
            s.AppendLine();
            s.AppendLine("- Character name: " + c.Name);
            s.AppendLine("- Character level: " + c.Level);
            s.AppendLine("- Strength: " + c.baseAttributes.strength);
            s.AppendLine("- Dexterity: " + c.baseAttributes.dexterinty);
            s.AppendLine("- Intelligence: " + c.baseAttributes.intelligense);
            s.AppendLine("- Damage: " + c.characterDemage());

            return s.ToString();
        }

        /// <summary>
        /// Character Leves up - primary attributes, wepons and amor updates
        /// </summary>
        /// <returns> int </returns>
        public int LevelUp()
        {
            this.Level++;
            PrimaryAttributes updateBase = getCharacterLevelup();
            this.baseAttributes = updateBase;
            return this.Level;
        }
    }
}
