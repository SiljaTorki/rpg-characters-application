﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class InvalidArmorException : Exception
    {
        /// <summary>
        /// 
        /// Exeption constructor
        /// 
        /// </summary>
        /// <param name="message"></param>
        public InvalidArmorException(string message) : base(message)
        {
        }

        /// <summary>
        /// 
        /// Override Message method for the exeption message
        /// 
        /// </summary>
        public override string Message => "You can not equip this item, it is not a armor item!";
    }
}
