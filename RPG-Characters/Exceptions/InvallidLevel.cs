﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class InvallidLevel : Exception
    {
        /// <summary>
        /// 
        /// Exeption constructor
        /// 
        /// </summary>
        /// <param name="message"></param>
        public InvallidLevel(string message) : base(message)
        {
        }

        /// <summary>
        /// 
        /// Override Message method for the exeption message
        /// 
        /// </summary>
        public override string Message => "You can not equip this item, the timen is being too high of a level requirement!";
    }
}
