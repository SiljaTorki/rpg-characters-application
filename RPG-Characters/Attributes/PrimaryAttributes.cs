﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class PrimaryAttributes
    {
        public double strength = 0;
        public double dexterinty = 0;
        public double intelligense = 0;

        public PrimaryAttributes(double strength = 0, double dexterinty = 0, double intelligense = 0)
        {
            this.strength = strength;
            this.dexterinty = dexterinty;
            this.intelligense = intelligense;
        }
        public static PrimaryAttributes operator +(PrimaryAttributes primary1, PrimaryAttributes primary2)
        {
            return new PrimaryAttributes
            {
                strength = primary1.strength + primary2.strength, 
                dexterinty = primary1.dexterinty + primary2.dexterinty, 
                intelligense = primary1.intelligense + primary2.intelligense
            };
        }

    }


   
}
