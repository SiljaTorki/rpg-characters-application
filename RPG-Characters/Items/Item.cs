﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public abstract class Item
    {
        private string name;
        public string Name { get { return name; } }
        public int RequierdLevel { get; set; }
        public SlotENUM Slot { get; set; }


        /// <summary>
        /// 
        /// Constructor for Item abstract class 
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="requierdLevel"></param>
        /// <param name="slot"></param>
        public Item(string name, int requierdLevel, SlotENUM slot)
        {
            this.name = name;
            this.RequierdLevel = requierdLevel;
            this.Slot = slot;
        }


        
    }
}
