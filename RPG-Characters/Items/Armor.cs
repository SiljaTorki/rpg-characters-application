﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class Armor: Item
    {
        private ArmorENUM Type { get; set; }
        public PrimaryAttributes Attributes { get; set; }

        /// <summary>
        /// 
        /// Constructor for Item subclass - Armor
        /// Sets base of armor to level 1 and slot to BODY
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="attribute"></param>
        /// <param name="level"></param>
        /// <param name="slot"></param>
        public Armor(ArmorENUM type, PrimaryAttributes attribute, int level = 1, SlotENUM slot = SlotENUM.BODY) : base("Armor - " + type.ToString(), level, slot)
        {
            this.Type = type;
            this.Attributes =attribute;
        }
    }
}
