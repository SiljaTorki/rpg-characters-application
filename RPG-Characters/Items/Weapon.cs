﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class Weapon : Item
    {
        private WeaponsENUM weapon { get; set; }
        private double damage { get; set; }
        private double attackSpeed { get; set; }

        /// <summary>
        /// 
        /// Constructor for Item subclass - Weapon
        /// Sets base of weapon to level 1 and slot to WEAPON
        /// 
        /// </summary>
        /// <param name="weapon"></param>
        /// <param name="damage"></param>
        /// <param name="attackSpeed"></param>
        /// <param name="level"></param>
        /// <param name="slot"></param>
        public Weapon(WeaponsENUM weapon, double damage, double attackSpeed, int level = 1, SlotENUM slot = SlotENUM.WEAPON) : base("Weapon - "+weapon.ToString(), level, slot)
        {
            this.weapon = weapon;
            this.damage = damage;
            this.attackSpeed = attackSpeed;
        }

        /// <summary>
        /// 
        /// Weapons have a base damage,
        /// and how many attacks per second can be performed with the weapon.
        /// The weapons damage per second(DPS) is calculated by multiplying these together
        /// 
        /// </summary>
        /// <returns> double </returns>
        public double DPS()
        {
            return damage * attackSpeed;
        }

    }
}
