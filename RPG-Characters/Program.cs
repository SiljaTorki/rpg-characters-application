﻿using System;

namespace RPG_Characters
{
    internal class Program
    {
        private static void Main(string[] args)
        {
           //Create new character - exaple with Warrior
            Character warrior = new Warrior("SuperWarrior");

            //Show warrior
            string show = warrior.showCharacter(warrior);
            Console.WriteLine(show);

            //Level up character
            int newLevel = warrior.LevelUp();
            Console.WriteLine("Level up: " + newLevel);
            Console.WriteLine();


            //Equip new weapon and armor
            Item newWeapon = new Weapon(WeaponsENUM.AXE, 3, 6, 2, SlotENUM.WEAPON);
            Item newArmor = new Armor(ArmorENUM.LEATHER, new PrimaryAttributes(2, 2, 2), 2, SlotENUM.HEAD);

            string equipWeapon = warrior.EquipItem(newWeapon);
            string equipArmor = warrior.EquipItem(newArmor);
            Console.WriteLine(equipWeapon);
            Console.WriteLine(equipArmor);
            Console.WriteLine();

            //Calculate character damage
            double calculateDamage = warrior.characterDemage();
            Console.WriteLine("Damage: " + calculateDamage);
            Console.WriteLine();

            //Display character after leveling up
            Console.WriteLine("Character after level up: ");
            string showAfterLevelUp = warrior.showCharacter(warrior);
            Console.WriteLine(showAfterLevelUp);

        }
    }
}
