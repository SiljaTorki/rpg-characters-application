﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    /// <summary>
    /// Enum class for the types of Armor that exist
    /// </summary>
    public enum ArmorENUM
    {
        CLOTH,
        LEATHER,
        MAIL,
        PLATE
    }
}
