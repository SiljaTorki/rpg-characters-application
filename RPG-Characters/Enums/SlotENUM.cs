﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    /// <summary>
    /// Enum class for the slots items can be equipped in 
    /// </summary>
    public enum SlotENUM
    {
        HEAD,
        BODY,
        LEGS,
        WEAPON
    }
}
