﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using RPG_Characters;

namespace RPG_Character_Tests
{
    public class ItemTests
    {
        /// <summary>
        /// If a character tries to equip a high level weapon, InvallidLevel is thrown.
        /// </summary>
        [Fact]
        public void EquipItem_EquipHighLevelWeapon_ThrowInvallidLevelException()
        {
            // Arrange
            Warrior warrior = new();
            Weapon testAxe = new(WeaponsENUM.AXE, 7, 1.1, 2);

            //Act and Assert
            Assert.Throws<InvallidLevel>(() => warrior.EquipItem(testAxe));
        }

        /// <summary>
        /// If a character tries to equip a high level armor piece, InvallidLevel is thrown.
        /// </summary>
        [Fact]
        public void EquipItem_EquipHighLevelArmor_ThrowInvallidLevelException()
        {
            //Arrange
            Warrior warrior = new();
            Armor testPlateBody = new(ArmorENUM.PLATE, new PrimaryAttributes(1, 0, 0), 2, SlotENUM.BODY);

            //Act and Assert
            Assert.Throws<InvallidLevel>(() => warrior.EquipItem(testPlateBody));

        }

        /// <summary>
        /// If a character tries to equip the wrong weapon type, InvalidWeaponException is thrown.
        /// </summary>
        [Fact]
        public void EquipItem_TryToEquipWrongWeaponType_ThrowInvalidWeaponException()
        {
            //Arrange
            Warrior warrior = new();
            Weapon testBow = new (WeaponsENUM.BOW, 12, 0.8, 1, SlotENUM.BODY);

            //Act and Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipItem(testBow));
        }

        /// <summary>
        /// If a character tries to equip the wrong armor type, InvalidArmorException is thrown.
        /// </summary>
        [Fact]
        public void EquipItem_TryToEquipWrongArmorType_ThrowInvalidArmorException()
        {
            //Arrange
            Warrior warrior = new();
            Armor cloth = new (ArmorENUM.CLOTH, new PrimaryAttributes(1, 1, 1), 1, SlotENUM.WEAPON);

            //Act and Assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipItem(cloth));
        }

        /// <summary>
        /// If a character equips a valid weapon, a success message is returned
        /// </summary>
        [Fact]
        public void EquipItem_EquipWeaponValid_ReturnSuccessMessage()
        {
            // Arrange
            Warrior warrior = new();
            Weapon testAxe = new(WeaponsENUM.AXE, 7, 1.1, 1);

            // Act
            string successMassage = "New weapon equipped!";
            string actualMessage = warrior.EquipItem(testAxe);

            // Assert
            Assert.Equal(actualMessage, successMassage);
        }

        /// <summary>
        /// If a character equips a valid armor piece, a success message is returned
        /// </summary>
        [Fact]
        public void EquipItem_EquipArmorValid_ReturnSuccessMessage()
        {
            // Arrange
            Warrior warrior = new();
            Armor testPlateBody = new(ArmorENUM.PLATE, new PrimaryAttributes(1, 0, 0), 1, SlotENUM.BODY);

            // Act
            string successMassage = "New armour equipped!";
            string actualMessage = warrior.EquipItem(testPlateBody);

            // Assert
            Assert.Equal(actualMessage, successMassage);
        }

        /// <summary>
        /// Calculate Damage if no weapon is equipped.
        /// </summary>
        [Fact]
        public void CharacterDemage_IfNoWeaponEquipped_CalculateDamage()
        {
            // Arrange
            Warrior warrior = new();
            double expextedDamage = 1 * (1 + (5 / 100));

            //Act
            double actualDamage = warrior.characterDemage();

            //Assert
            Assert.Equal(expextedDamage, actualDamage);
        }

        /// <summary>
        /// Calculate Damage with valid weapon equipped.
        /// </summary>
        [Fact]
        public void CharacterDemage_IfEquipWeaponValid_CalculateDamage()
        {
            // Arrange
            Warrior warrior = new();
            Weapon testAxe = new(WeaponsENUM.AXE, 7, 1.1, 1);
            double expextedDamage = (7 * 1.1) * (1 + (5 / 100));

            //Act
            warrior.EquipItem(testAxe);
            double actualDamage = warrior.characterDemage();

            //Assert
            Assert.Equal(expextedDamage, actualDamage);
        }

        /// <summary>
        /// Calculate Damage with valid weapon and armor equipped.
        /// </summary>
        [Fact]
        public void CharacterDemage_IfEquipWeaponValidAndEquipArmorValid_CalculateDamage()
        {
            // Arrange
            Warrior warrior = new();
            Weapon testAxe = new(WeaponsENUM.AXE, 7, 1.1, 1);
            Armor testPlateBody = new(ArmorENUM.PLATE, new PrimaryAttributes(1, 0, 0), 1, SlotENUM.BODY);
            double expextedDamage = (7 * 1.1) * (1 + ((5 + 1) / 100));

            //Act
            warrior.EquipItem(testAxe);
            warrior.EquipItem(testPlateBody);
            double actualDamage = warrior.characterDemage();

            //Assert
            Assert.Equal(expextedDamage, actualDamage);
        }
    }
}
