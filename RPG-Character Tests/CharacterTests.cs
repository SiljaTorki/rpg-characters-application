﻿using RPG_Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;


namespace RPG_Character_Tests
{
    public class CharacterTests
    {
        /// <summary>
        /// A character is level 1 when created.
        /// </summary>
        [Fact]
        public void Cunstrctor_CharacterLevel_IsLevelOneWhenCreated()
        {
            // Arrange
            int expectedLevel = 1;
            Warrior characterObj = new();

            // Act
            int level = characterObj.Level;

            // Asserrt
            Assert.Equal(expectedLevel, level);
        }

        /// <summary>
        /// When a character gains a level, it should be level 2.
        /// </summary>
        [Fact]
        public void LevelUp_CharacterLevelsUp_CharacterGainsLevel()
        {
            // Arrange
            int expectedLevel = 2;
            Warrior characterObj = new();

            // Act
            int level = characterObj.LevelUp();

            // Asserrt
            Assert.Equal(expectedLevel, level);

        }


        /// <summary>
        /// Each character class is created with the proper default attributes.
        /// Four test methods for eatch character type
        /// </summary>

        //Mage Test
        [Fact]
        public void Cunstructon_CharacterBase_MageHasProperDefaultAttributes()
        {
            // Arrange
            Mage characterObj = new();
            double[] expectedAttributes = { 1, 1, 8 };

            // Act
            double[] actualAttributes = { characterObj.baseAttributes.strength, characterObj.baseAttributes.dexterinty, characterObj.baseAttributes.intelligense };

            // Asserrt
            Assert.Equal(expectedAttributes, actualAttributes);
            

        }

        //Ranger Test
        [Fact]
        public void Cunstructon_CharacterBase_RangerHasProperDefaultAttributes()
        {
            // Arrange
            Ranger characterObj = new();
            double[] expectedAttributes = { 1, 7, 1 };

            // Act
            double[] actualAttributes = { characterObj.baseAttributes.strength, characterObj.baseAttributes.dexterinty, characterObj.baseAttributes.intelligense };

            // Asserrt
            Assert.Equal(expectedAttributes, actualAttributes);


        }

        //Rouge Test
        [Fact]
        public void Cunstructon_CharacterBase_RogueHasProperDefaultAttributes()
        {
            // Arrange
            Rogue characterObj = new();
            double[] expectedAttributes = { 2, 6, 1 };

            // Act
            double[] actualAttributes = { characterObj.baseAttributes.strength, characterObj.baseAttributes.dexterinty, characterObj.baseAttributes.intelligense };

            // Asserrt
            Assert.Equal(expectedAttributes, actualAttributes);


        }

        //Warrior Test
        [Fact]
        public void Cunstructon_CharacterBase_WarriorHasProperDefaultAttributes()
        {
            // Arrange
            Warrior characterObj = new();
            double[] expectedAttributes = { 5, 2, 1 };

            // Act
            double[] actualAttributes = { characterObj.baseAttributes.strength, characterObj.baseAttributes.dexterinty, characterObj.baseAttributes.intelligense };

            // Asserrt
            Assert.Equal(expectedAttributes, actualAttributes);


        }


        /// <summary>
        /// Each character class has their attributes increased when leveling up.
        /// </summary>
        //Mage Test
        [Fact]
        public void LevelUp_CharacterLevelUpAttributes_MageHasProperLevelUpAttributes()
        {
            // Arrange
            double[] expectedAttributes = { 2, 2, 13 } ;
            Mage characterObj = new();

            // Act
            characterObj.LevelUp();
            double[] actualAttributes = { characterObj.baseAttributes.strength, characterObj.baseAttributes.dexterinty, characterObj.baseAttributes.intelligense };

            // Asserrt
            Assert.Equal(expectedAttributes, actualAttributes);
           

        }

        //Ranger Test
        [Fact]
        public void LevelUp_CharacterLevelUpAttributes_RangerHasProperLevelUpAttributes()
        {
            // Arrange
            double[] expectedAttributes = { 2, 12, 2 };
            Ranger characterObj = new();

            // Act
            characterObj.LevelUp();
            double[] actualAttributes = { characterObj.baseAttributes.strength, characterObj.baseAttributes.dexterinty, characterObj.baseAttributes.intelligense };

            // Asserrt
            Assert.Equal(expectedAttributes, actualAttributes);


        }

        //Rouge Test
        [Fact]
        public void LevelUp_CharacterLevelUpAttributes_RogueHasProperLevelUpAttributes()
        {
            // Arrange
            double[] expectedAttributes = { 3, 10, 2 };
            Rogue characterObj = new();

            // Act
            characterObj.LevelUp();
            double[] actualAttributes = { characterObj.baseAttributes.strength, characterObj.baseAttributes.dexterinty, characterObj.baseAttributes.intelligense };

            // Asserrt
            Assert.Equal(expectedAttributes, actualAttributes);
        }

        //Warrior Test
        [Fact]
        public void LevelUp_CharacterLevelUpAttributes_WarriorHasProperLevelUpAttributes()
        {
            // Arrange
            double[] expectedAttributes = { 8, 4, 2 };
            Warrior characterObj = new();

            // Act
            characterObj.LevelUp();
            double[] actualAttributes = { characterObj.baseAttributes.strength, characterObj.baseAttributes.dexterinty, characterObj.baseAttributes.intelligense };

            // Asserrt
            Assert.Equal(expectedAttributes, actualAttributes);


        }


    }
}
