# RPG Characters Application

A console application in C#

## Table of Contents
- [Installation](#installation)
- [Usage](#usage)
- [Description](#description)
- [Maintainers](#maintainers)
- [Visuals](#visuals)
- [Project status](#project-status)


## Installation
Project is created with:

* Visual Studio 2019 with .NET 5

## Usage

- ``` git clone git@gitlab.com:SiljaTorki/rpg-characters-application.git ```
- Open in Visual Studio 2019 by clicking the ***RPG-Characters.sln*** file
- Run program with *F5* or   ![Run Button](/Images/RunProgram.PNG)
- Run unit tests by rigth cilck ***RPG-Character Tests*** then click ***Run Tests***


## Description
In the application the character can be four classes:

* Mage
* Ranger
* Rouge
* Warrior

The characters has types of attributes, which represent different aspects of the character. They are 
represented as Primary attributes.
Primary attributes are what determine the characters power, as the character levels up their primary attributes 
increase.

The primary attributes:

* Strength � determines the physical strength of the character.
* Dexterity � determines the characters ability to attack with speed and nimbleness.
* Intelligence � determines the characters affinity with magic.

When a character is created, they are provided a name. Every character begins at level 1 and evry time a character levvels up they gain primary attributes and get acces to new weapons, armors and equipment.

Weapons have a base damage, and how many attacks per second can be performed with the weapon. The weapons 
damage per second (DPS) is calculated by multiplying these together.
*  DPS = Damage * Attack speed

Armor has attributes that add to the character�s power. These attributes are the same as the primary attributes and this 
means the PrimaryAttribute custom type can be reused.

A character can equip any item. An equipped item is stored in the character�s equipment and is used to increase the 
characters power.

## Maintainers
Silja Stubhag Torkildsen - @SiljaTorki

## Visuals 
### Class Diagram

![Class Diagram](/Images/ClassDiagram.PNG)

## Project status
Complete!